from guardian import GuardState, GuardStateDecorator
import time

####
# Presently not written to be controlled by ISC_Lock, but probably can/should be.
####


# Define all QUAD suspensions
quads = ['ITMX','ITMY','ETMX','ETMY']
# Define Bounce and roll damping modes
dofs = ['V', 'R']
dofs_word = ['BOUNCE', 'ROLL']

# Nominal operational state
nominal = 'DAMP'

class INIT(GuardState):
    index = 0
    def main(self):
        return True

class IDLE(GuardState):
    index = 1
    goto = True
    request = True
    def main(self):
        for dof in dofs:
            for quad in quads:
                ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_GAIN'] = 0
        return True
    def run(self):
        # Disable Guardian from automatically proceeding.
        return True
        
        # if (ezca['GRD-ISC_LOCK_STATE_N'] > 1600): # Looking back at trends, by the end of turning on Cal lines, all the readouts of the bounce/roll modes is clean enough to use.
            # log("Bounce Roll Damping Ready to begin prepare filters.")
            # return True

class RESET_FITLERS(GuardState):
    index = 5
    def main(self):
        # Clear monitor filter histories for all QUADs
        for mode in dofs_word:
            for quad in quads:
                ezca[f'SUS-{mode}_{sus}_RSET'] = 2
            ezca[f'SUS-{mode}_ALL_RSET'] = 2
        
        # Initialize  damping filter banks to a base starting point.
        for quad in quads: #First turn all Gains to Zero
            for dof in dofs:
                ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_TRAMP'] = 3
                time.sleep(0.1)
                ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_GAIN'] = 0
        time.sleep(3) #Wait out ramps.
        for quad in quads:
            ezca[f'SUS-{quad}_M0_DARM_DAMP_INPUT_MTRX_1_1'] =1000 # boosts Bounce noisefloor from 2e-6 to 2e-3
            ezca[f'SUS-{quad}_M0_DARM_DAMP_INPUT_MTRX_2_1'] =1000 # boosts Roll noisefloor from 3e-7 to 3e-4
            ezca[f'SUS-{quad}_M0_DARM_DAMP_INPUT_MTRX_1_2'] =0
            ezca[f'SUS-{quad}_M0_DARM_DAMP_INPUT_MTRX_2_2'] =0
            for dof in dofs:
                ezca.switch(f'SUS-{quad}_M0_DARM_DAMP_{dof}', 'FM1', 'OFF', 'FM2', 'OFF', 'FM3', 'OFF', 'FM4', 'OFF', 'FM5', 'OFF', 'FM6', 'ON', 'FM7', 'OFF', 'FM8', 'OFF', 'FM9', 'OFF', 'FM10', 'OFF') # Turn off all filters but leave BP on (FM6).
                time.sleep(0.1)
        time.sleep(7) # Wait for filters to switch
        for quad in quads: 
            for dof in dofs:
                ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_RSET'] = 2
        # ITMY Roll damping diabled for now, since it is not identified.
        ezca[f'SUS-{quad}_M0_DARM_DAMP_INPUT_MTRX_2_1'] = 0
        
        # Enable Output, note that Gains is set to zero in the above for loop.
        ezca.switch(f'SUS-{quad}_M0_DARM_DAMP_{dof}', 'OUTPUT', 'ON')
        
        return True
    def run(self):
        if (ezca['GRD-ISC_LOCK_STATE_N'] == 2000) :
            log("Bounce Roll Damping Ready to damp.")
            return True

class DAMP(GuardState):
    index = 10
    goto = True
    request = True
    
    def main(self):
        # This is edged to itself so it can rerun. Therefore start with turning off gains before playing with settings:
        ezca['SUS-ITMX_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ITMX_M0_DARM_DAMP_R_GAIN'] = 0
        ezca['SUS-ITMY_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ITMY_M0_DARM_DAMP_R_GAIN'] = 0
        ezca['SUS-ETMX_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ETMX_M0_DARM_DAMP_R_GAIN'] = 0
        ezca['SUS-ETMY_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ETMY_M0_DARM_DAMP_R_GAIN'] = 0
        time.sleep(3.0)
        # Set ITMX damping filters phase
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_V', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_V', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_V', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_V', 'FM4', 'ON') # -30
        
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_R', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_R', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_R', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ITMX_M0_DARM_DAMP_R', 'FM4', 'ON') # -30
        
        
        # Set ITMY damping filters phase
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_V', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_V', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_V', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_V', 'FM4', 'ON') # -30
        
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_R', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_R', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_R', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ITMY_M0_DARM_DAMP_R', 'FM4', 'ON') # -30
        
        
        # Set ETMX damping filters phase
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_V', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_V', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_V', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_V', 'FM4', 'ON') # -30
        
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_R', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_R', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_R', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ETMX_M0_DARM_DAMP_R', 'FM4', 'ON') # -30
        
        
        # Set ETMY damping filters phase
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_V', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_V', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_V', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_V', 'FM4', 'ON') # -30
        
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_R', 'FM1', 'ON') # +60
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_R', 'FM2', 'ON') # -60
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_R', 'FM3', 'ON') # +30
        # ezca.switch('SUS-ETMY_M0_DARM_DAMP_R', 'FM4', 'ON') # -30
        
        # Above filters all have a 7 second ramp time built into them.
        time.sleep(7.0)
        
        ezca['SUS-ITMX_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ITMX_M0_DARM_DAMP_R_GAIN'] = 0
        
        ezca['SUS-ITMY_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ITMY_M0_DARM_DAMP_R_GAIN'] = 0
        
        ezca['SUS-ETMX_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ETMX_M0_DARM_DAMP_R_GAIN'] = 0
        
        ezca['SUS-ETMY_M0_DARM_DAMP_V_GAIN'] = 0
        ezca['SUS-ETMY_M0_DARM_DAMP_R_GAIN'] = 0
        
        log("Bounce roll mode damping engaged")
        time.sleep(3.0)
        return True
    
    def run(self):
        if (ezca['GRD-ISC_LOCK_STATE_N'] < 1600) :
            log("Bounce Roll Damping exit! Only avaialbe in in later locking stages")
            return 'IDLE'
        # This block considers all the modes and can react in seconds to egregious filter setting errors.
        for mode in dofs_word:
            level = ezca[f'SUS-{mode}_ALL_RMS_LOG10_OUTMON'] # Prone to Glitches
            threshold = 1.5 if mode == 'BOUNCE' else  1.0
            if (level >= threshold):
                notify(f'Warning! Large {mode} elevation. All Damping off!')
                return 'UNDAMP'
        # Below considers independant mode monitors. Note that they have a very narrow band filters and will not react quickly enough to rapid changes
        for sus in quads:
            for mode in dofs_word:
                level = ezca['SUS-'+mode+"_"+sus+'_RMS_LOG10_OUTMON']
                threshold = 1.2 if mode == 'BOUNCE' else  0.5
                if (level >= threshold):
                    # Rather than escape to UNDAMP state, we can turn off the individual filter here. If growtih persists, the all-modes check will kick us there.
                    notify(f'Warning! {sus} {mode} elevated. Individual Damping off!')
                    ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_GAIN'] = 0
        return True

class PLAYGROUND(GuardState):
    index = 20
    goto = True
    request = True
    redirect = True
    
    def main(self):
        '''
        This state is designed to sit in and be able to alter settings on filters, with it still having the 
        safety net effect in catching ring-ups.
        
        Alther the thresholds in the run state apporpriately to make your work do-able.
        '''
        for sus in quads:
            for mode in dofs:
                # Zero the gains for all QUADs
                ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_GAIN'] = 0
        log("Bounce roll mode damping disengaged")
        time.sleep(3.0)
        for sus in quads:
            for mode in dofs:
                ezca.switch(f'SUS-{quad}_M0_DARM_DAMP_{dof}', 'OUTPUT', 'OFF')
        return True
    
    def run(self):
        # You can sit here and play with filter settings and it will catch ringups and stop damping.
        if (ezca['GRD-ISC_LOCK_STATE_N'] < 1600) :
            log("Bounce Roll Damping exit! Only avaialbe in in later locking stages")
            return 'IDLE'
        # This block considers all the modes and can react in seconds to egregious filter setting errors.
        for mode in dofs_word:
            level = ezca[f'SUS-{mode}_ALL_RMS_LOG10_OUTMON'] # Prone to Glitches
            threshold = 1.5 if mode == 'BOUNCE' else 1.0 
            if (level >= threshold):
                notify(f'Warning! Large {mode} elevation. Damping off!')
                return 'UNDAMP'
        # Below considers independant mode monitors. Note that they have a very narrow band filters and will not react quickly enough to rapid changes
        for sus in quads:
            for mode in dofs_word:
                level = ezca['SUS-'+mode+"_"+sus+'_RMS_LOG10_OUTMON']
                threshold = 1.2 if mode == 'BOUNCE' else  0.5
                if (level >= threshold):
                    notify(f'Warning! {sus} {mode} elevated. Individual Damping off!')
                    ezca[f'SUS-{quad}_M0_DARM_DAMP_{dof}_GAIN'] = 0
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','RESET_FITLERS'),
    ('RESET_FITLERS','DAMP'),
    ('RESET_FITLERS','PLAYGROUND'),
    ('DAMP','PLAYGROUND'),
    ('PLAYGROUND','IDLE'),
    ]

edges += [('DAMP','DAMP')]
edges += [('PLAYGROUND','PLAYGROUND')]

